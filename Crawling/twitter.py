import tweepy
import time
import json
import os

#circa 450/500 processamenti al minuto
class MyStreamListener(tweepy.StreamListener):
 cur = 0
 
 def __init__(self):
  super().__init__()
  self.topic= "science"
  self.f = open(self.topic+"_"+"tweets.txt", "a")
  self.start = time.time()
  self.processati = 0
  
 def on_status(self, status):
  self.processati = self.processati+1
  print(self.processati)
  ess = status._json
  ess["topic"] = self.topic
  result = json.dumps(ess)

  self.f.write(result)
  self.f.write("\r\n")
  self.f.flush()
 


 def on_error(self, status_code):
  print('Got an error with status code: ' + str(status_code))
  return True

 def on_timeout(self):
  print('Timeout...')
  return True 


consumer_key = ""
consumer_key_secret = ""
access_token = ""
access_token_secret = ""

words = ["science"]
other=["politics","science","music","sport","hobby","technology"]

auth = tweepy.OAuthHandler(consumer_key, consumer_key_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)
myStreamListener = MyStreamListener()
myStream = tweepy.Stream(auth=api.auth, listener=myStreamListener)
myStream.filter(languages=["en"],track=words,is_async=True)


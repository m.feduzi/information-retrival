import tweepy
import time
import json
import os
from elasticsearch import Elasticsearch

filepath = '../Crawling/'
cnt = 1
files = os.listdir("../Crawling")
es = Elasticsearch("localhost:9200")

for f in files:
 if f != "twitter.py" and f != "old":
  try:
   with open(filepath+f, 'r') as fp:
    line = fp.readline()
    while line:
     line = fp.readline()
     tweet = json.loads(line.strip())
     print("***********************************************************" + str(cnt))
     t = {}
     t["tweet_text"] = tweet["text"]
     t["topic"] = tweet["topic"]
     t["created"] = time.strftime('%Y-%m-%d', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
     t["user"] = tweet["user"]["screen_name"]
     if tweet["place"] is not None:
      t["country"] = tweet["place"]["country_code"]
     insert = es.index(index="tweets",body=t)
     print(json.dumps(t, indent=1))
     cnt += 1
  except:
   print("errore")

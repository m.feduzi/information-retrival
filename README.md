1) Per far partire elasticsearch e kibana occorre spostarsi nella cartella del progetto ed eseguire il comando:
    docker-compose up -d
2) Attraverso kibana, o tramite curl, eseguire il seguente comando per configurare il mapping:
PUT /tweets
{
  "mappings": {
    "properties": {
      "tweet_text": {
        "type": "text",
        "analyzer":"custom_analyzer",
        "search_analyzer": "custom_analyzer"
      },
      "user": {"type": "keyword"},
      "created": {"type": "date" },
      "topic": {"type": "keyword"},
      "country": {"type": "keyword"}
    }
  },
  "settings": {
    "analysis": {
      "analyzer": {
        "custom_analyzer": {
          "type": "custom",
          "char_filter": ["replace_emoticon"],
          "tokenizer": "standard_url_email",
          "filter": ["stop_word_removal",
                     "stemming",
                     "lowercase"]
        }
      },
      "tokenizer":{
        "standard_url_email": {
          "type": "uax_url_email"
        }
      },
      "char_filter":{
        "replace_emoticon": {
          "type": "mapping",
          "mappings": [
            ":) => happy",
            ":( => sad",
            ":'( => cry",
            ":D => laugh",
            ":O => surprise",
            ":* => kiss",
            "%) => confused",
            ":| => indecision",
            "O:) => angel",
            "|;‑) => cool"
          ]
        }
      },
      "filter":{
        "stemming": {
          "type": "stemmer",
          "name": "english"
        },
        "stop_word_removal": {
          "type": "stop",
          "stopwords": "_english_"
        }
      }
    }
  }
}
3) Per caricare i tweet scaricati in elasticsearch occorre spostarsi nella folder Preparing ed eseguire il comando: 
     python twitter.py
4) Spostarsi nella cartella principale del progetto ed eseguire il comando per far partire l'interfaccia grafica:
     python app.py
5) Accedere a http://localhost ed iniziare la ricerca
from flask import Flask, render_template, request, url_for, flash, redirect # pip install afinn
from afinn import Afinn # pip install afinn
from nltk.tokenize import word_tokenize # pip install nltk
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords
from nltk import wordpunct_tokenize
from nltk.stem.lancaster import LancasterStemmer
from collections import Counter
from elasticsearch import Elasticsearch # pip install elasticsearch
import numpy as np
import pandas as pd
import itertools
import re
import string
import datetime
import math
import os
import socket
import json

# regular expression for token preprocessing
emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs

    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

# obtain path to read tweets of a user and build his bag of words
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
# instantiate elasticsearch instance
es = Elasticsearch("localhost:9200")

def tokenize(s):
    return tokens_re.findall(s)

def preprocess(s, lowercase=True):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens

def remove_emoji(string):
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           u"\U0001F900-\U0001F9FF"
                           u"\U0001F300−\U0001F5FF"
                           "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r'', string)

def preprocessingtext(text):
    # remove emoji and other particular character
    text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', text, flags=re.MULTILINE)
    text = remove_emoji(text)
    # normalization: put to lowercase every word and remove emoticons
    tweets_tokenized = preprocess(text)
    # stopword removal
    stop =stopwords.words('english')
    stop =set(stop)
    # adding some of the stopwords after observing the tweets
    stop.add("the")
    stop.add("and")
    stop.add("we")
    stop.add("i")
    stop.add("j")
    stop.add("k")
    stop.add("i'd")
    stop.add("that's")
    stop.add("\x81")
    stop.add("it")
    stop.add("i'm")
    stop.add("i'll")
    stop.add("i've")
    stop.add("...")
    stop.add("\x89")
    stop.add("ĚĄ")
    stop.add("it's")
    stop.add("ă")
    stop.add("\x9d")
    stop.add("âÂĺ")
    stop.add("Ě")
    stop.add("˘")
    stop.add("Â")
    stop.add("âÂ")
    stop.add("Ň")
    stop.add("http")
    stop.add("https")
    stop.add("co")
    stop.add("000")
    stop.add("Ň")
    stop.add("Ň")
    stop.add("Ň")
    # insert in a stoplist other words that are not remove by stopwords english
    # because that list remove online stopword that init with ad uppercase character
    stop.add("i")
    stop.add("me")
    stop.add("my")
    stop.add("myself")
    stop.add("we")
    stop.add("our")
    stop.add("ours")
    stop.add("ourselves")
    stop.add("you")
    stop.add("your")
    stop.add("yours")
    stop.add("yourself")
    stop.add("yourselves")
    stop.add("he")
    stop.add("him")
    stop.add("his")
    stop.add("himself")
    stop.add("she")
    stop.add("her")
    stop.add("hers")
    stop.add("herself")
    stop.add("it")
    stop.add("its")
    stop.add("itself")
    stop.add("they")
    stop.add("them")
    stop.add("their")
    stop.add("theirs")
    stop.add("themselves")
    stop.add("what")
    stop.add("which")
    stop.add("who")
    stop.add("whom")
    stop.add("this")
    stop.add("that")
    stop.add("these")
    stop.add("those")
    stop.add("am")
    stop.add("is")
    stop.add("are")
    stop.add("was")
    stop.add("were")
    stop.add("be")
    stop.add("been")
    stop.add("being")
    stop.add("have")
    stop.add("has")
    stop.add("had")
    stop.add("having")
    stop.add("do")
    stop.add("does")
    stop.add("did")
    stop.add("doing")
    stop.add("a")
    stop.add("an")
    stop.add("the")
    stop.add("and")
    stop.add("but")
    stop.add("if")
    stop.add("or")
    stop.add("because")
    stop.add("as")
    stop.add("until")
    stop.add("while")
    stop.add("of")
    stop.add("at")
    stop.add("by")
    stop.add("for")
    stop.add("with")
    stop.add("about")
    stop.add("against")
    stop.add("between")
    stop.add("into")
    stop.add("through")
    stop.add("during")
    stop.add("before")
    stop.add("after")
    stop.add("above")
    stop.add("below")
    stop.add("to")
    stop.add("from")
    stop.add("up")
    stop.add("down")
    stop.add("in")
    stop.add("out")
    stop.add("on")
    stop.add("off")
    stop.add("over")
    stop.add("under")
    stop.add("again")
    stop.add("further")
    stop.add("then")
    stop.add("once")
    stop.add("here")
    stop.add("there")
    stop.add("when")
    stop.add("where")
    stop.add("why")
    stop.add("how")
    stop.add("all")
    stop.add("any")
    stop.add("both")
    stop.add("each")
    stop.add("few")
    stop.add("more")
    stop.add("most")
    stop.add("other")
    stop.add("some")
    stop.add("such")
    stop.add("no")
    stop.add("nor")
    stop.add("not")
    stop.add("only")
    stop.add("own")
    stop.add("same")
    stop.add("so")
    stop.add("than")
    stop.add("too")
    stop.add("very")
    stop.add("s")
    stop.add("t")
    stop.add("can")
    stop.add("will")
    stop.add("just")
    stop.add("don")
    stop.add("should")
    stop.add("now")
    stop.add("new")
    stop.add("example1")
    stop.add("amp")
    stop.add("get")
    stop.add("rt")
    # remove time directions/signs
    stop.add("am")
    stop.add("pm")
    #remove punctuation as a stopword
    stop.add("…")
    stop.add("..")
    stop.add(".")
    stop.add(",")
    stop.add("!")
    stop.add("?")
    stop.add("'")
    stop.add(":")
    stop.add("<")
    stop.add(">")
    stop.add(";")
    stop.add("\"")
    stop.add("/")
    stop.add("’")
    stop.add("-")
    stop.add(")")
    stop.add("(")
    stop.add("‼")
    stop.add("‼")
    stop.add("“")
    stop.add("”")
    stop.add("&")
    stop.add("@")
    stop.add("£")
    stop.add(" ")
    stop.add("")
    # remove singular number
    stop.add("1")
    stop.add("2")
    stop.add("3")
    stop.add("4")
    stop.add("5")
    stop.add("6")
    stop.add("7")
    stop.add("8")
    stop.add("9")
    stop.add("0")
    stop = list(stop)
    # Stop word removal
    tweets_tokenized_stop = [item for item in tweets_tokenized if item not in stop]
    # remove punctuation
    punctuation = string.punctuation
    tweets_tokenized_stop_punct = [item for item in tweets_tokenized_stop if item not in punctuation]
    # remove words of length 1
    tweets_tokenized_bow = [item for item in tweets_tokenized_stop_punct if len(item) > 1]
    # stemming
    #lancaster_stemmer = LancasterStemmer()
    #tweets_tokenized_new_stem = [lancaster_stemmer.stem(item) for item in tweets_tokenized_stop_punct]
    #print(tweets_tokenized_new_stem)
    return tweets_tokenized_bow

def create_bow(input_txt):
    f = open(os.path.join(APP_ROOT,input_txt), "r",  encoding="utf-8")
    lines = f.read()
    f.close()
    preprocess_text=preprocessingtext(lines)
    c = Counter(list(preprocess_text))
    return c


date = datetime.date.today()

fields = ["tweet_text","topic","created","user","country"]

app = Flask(__name__)
app.config['SECRET_KEY'] = '0742df9b0f132ad49e110398d635a39c'
app.config['TESTING'] = True
app.config['DEBUG'] = True

filter = ""
posts=[]
bow=[]
user_request = ""
country = ""

countries = {"Boris_Jonson":"GB","DonDiablo":"NL","Horizon_Hobby":"FR","ibm":"","KingJames":"US","waynerooney":"GB","katyperry":"","iliketomakestuf":"CN","jimmyfallon":"","mtbarra":"IT","":""}

@app.route("/",methods=['GET','POST'])
def home():
    return render_template('home.html', title='HOME', posts=posts, text="", user = user_request, filter="", page=1, num_page=0, bow=bow, country=country)

@app.route("/search",methods=['GET','POST'])
def search():
    if request.method == 'POST':

        # Retrieve usefull data and define data structure
        must = []
        should = []
        posts = []
        bow = []
        str = ""
        country = ""
        filter = ""
        filter=request.form['filter']
        query = request.form['query']
        user_request = request.form['user']
        user_request.replace(".txt","")
        country = countries[user_request]

        # take user selected and create his bag of words
        # than extract his common words in bag of words that
        # define his interests.
        # For simplify computation we compute Bag of words for every single query
        # we compute Bag of words every time because we assume that interests of user can
        # change over time though crawling of other tweets.
        if user_request != "":
            bow_total = create_bow("ProfiliUtente/profili/"+user_request+".txt")
            bow = list(bow_total.most_common(10))
            str = ' '.join(e[0] for e in bow)
            bow = str.split()
        text_query = query
        print(str)

        # boolean query
        # case AND, OR, NOT

        # Filter by mapping of elasticsearch
        # example of query: user: zestyopolis created:2020-01-08 topic: science with Boris_Jonson vs. DonDiablo
        # another example: user:AdobeStockSale
        # another example: user:fon_za09 tweet_text:https://t.co/YT9lVt3Eb0

        # Find match_prhase in query
        # example of query: "Donald Trump"
        # "Donald Trump" Iran    v.s.   Donald Trump Iran

        must.append({"query_string":{ "query":text_query, "default_field": "tweet_text"}})

        print(text_query)

        # insert must and should list of match and term for query

        if country != "":
            should.append({"term": {"country": country}})

        if filter != "All" and filter != "" :
            must.append({"term": {"topic": filter}})

        if str != "" and text_query.replace(" ","") != "":
            should.append({"match": {"tweet_text": str}})

        #print(should)
        #print(must)
        q = {"must": must,"should": should}

        # Take the user's parameters and put them into a
        # Python dictionary structured as an Elasticsearch query:
        query_body = {
          "size":300,
          "query": {
            "function_score": {
              "query": {
                "bool": q
              },
              "linear": {
                "created": {
                  "origin": "2020-01-13",
                  "scale":"9d",
                  "offset":"1d",
                  "decay":"0.6"
                }
              }
            }
          }

        }
        print(query_body)


        # Pass the query dictionary to the 'body' parameter of the
        # client's Search() method, and have it return results:
        result = es.search(index="tweets", body=query_body)
        res = result['hits']['hits']
        content = list()
        users = list()
        # remove result that are duplicate by text.
        for post in res:
            if post["_source"]["tweet_text"] not in content:# and post["_source"]["user"] not in ids:
                t = post["_source"]["tweet_text"]
                content.append(t)
                #users.append(post["_source"]["user"])
                post["_source"]["tweet_text"].replace("&amp","")
                posts.append(post)

        # calculate number of pages and divide results in pages (10 per page)
        num_page = math.ceil(len(posts)/10)
        page=1;

        return render_template('search.html',posts=posts,title='filter',text=query, filter=filter, page=page, num_page=num_page, user = user_request, bow=bow, country=country)
    return render_template('template.html', title='HOME')


if __name__ == "__main__":
    app.run( port=80, host='0.0.0.0')
